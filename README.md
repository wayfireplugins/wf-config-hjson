# Hjson Config Backend Plugin for Wayfire

This plugin provides the hjson config backend as alternative to the default ini backend

## Compile and install
You should have first compiled and installed wlroots, wf-config and wayfire.

- Get the sources
  - `git clone https://gitlab.com/wayfireplugins/wf-config-hjson`
- Enter the `wf-config-hjson`
  - `cd wf-config-hjson`
- Configure the project - we use meson for project management
  - `meson build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  - `ninja -C build -k 0 -j $(nproc) && sudo ninja -C build install`

## Usage
Start wayfire using this command
```
wayfire --config-backend /path/to/wayfire/plugins/libhjson-config-backend.so --config /path/to/wayfire.hjson
```

## Convert Ini to Hjson
This repo comes with a simple tool to convert between INI and Hjson formats.
To ceonvert from ini to hjson:
```
ini2hjson --ini-to-hjson /path/to/wayfire.ini /path/to/wayfire.hjson
```

Note: this binary is not installed to any location. You'll find it in the build directory.
