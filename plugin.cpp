/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <unistd.h>

#include <condition_variable>
#include <wayfire/config-backend.hpp>
#include <wayfire/core.hpp>
#include <wayfire/debug.hpp>
#include <wayfire/output.hpp>
#include <wayfire/plugin.hpp>
#include <wayfire/util/log.hpp>

#include "codec/codec.hpp"

typedef wf::config::config_manager_t WfConfigManager;

class HjsonConfigBackend : public wf::config_backend_t {
    public:
        void init( wl_display *display, WfConfigManager& config, const std::string& ) override;

    private:
        HjsonCodec *codec;
};

void HjsonConfigBackend::init( wl_display *display, WfConfigManager& config, const std::string& usrCfg ) {
    codec = new HjsonCodec( &config, false );

    /**
     * Initialize the configs
     * get_xml_dirs() will include all the env vars and return a list of dirs
     * If we set @sysConf to "", then it will automatically read the default
     * usrCfg can be empty.
     */
    codec->initialize( get_xml_dirs(), "", usrCfg );

    /** Add the inotify fd to wayland event loop to watch for ready-for-reading events */
    wl_event_loop_add_fd(
        wl_display_get_event_loop( display ),   // Event loop where this the inotifyFD will be watched for.
        codec->inotifyFD,                       // The inotify FD
        WL_EVENT_READABLE,                      // What event is being watched for: wait until there is
                                                // something to read
        codec->handleInotifyEvents,             // The callback function when the event is emitted
        codec                                   // User data
    );
}


DECLARE_WAYFIRE_CONFIG_BACKEND( HjsonConfigBackend );
