/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <iostream>
#include <set>

#include <cstring>
#include <cstdlib>
#include <unistd.h>

#include <wayfire/config.h>
#include <wayfire/config/file.hpp>
#include <wayfire/config/config-manager.hpp>
#include "codec/codec.hpp"

void usage() {
    std::cout << "ini2hjson v0.1.0" << std::endl << std::endl;

    std::cout << "Usage:" << std::endl;
    std::cout << "    ini2hjson [opts] infile outfile" << std::endl << std::endl;

    std::cout << "Options:" << std::endl;
    std::cout << "    --ini-to-hjson    Convert config stored as INI to hjson" << std::endl;
    std::cout << "    --hjson-to-ini    Convert config stored as hjson to INI" << std::endl << std::endl;
}


int main( int argc, char *argv [] ) {
    if ( argc != 4 ) {
        usage();
        return 1;
    }

    bool ini2hj = (strcmp( argv[ 1 ], "--ini-to-hjson" ) == 0);
    bool hj2ini = (strcmp( argv[ 1 ], "--hjson-to-ini" ) == 0);

    if ( not (ini2hj or hj2ini) ) {
        usage();
        return 1;
    }

    /** Source file: argv[ 2 ] */
    std::string input = argv[ 2 ];

    /** Output file: argv[ 3 ] */
    std::string output = argv[ 3 ];

    /** Convert hjson to config_manager_t */
    wf::config::config_manager_t hjCfgMgr;

    /** Conversions:
     * (A) Ini to Hjson Conversion
     *     Step 1: Write hjson to temporary file
     *     Step 2: Read the hjson into hjCfgMgr
     *     Step 3: Save the hjson into hjson style @output
     * (B) Hjson to Ini Conversion
     *     Step 1: Read the hjson into hjCfgMgr
     *     Step 2: Save the hjson into hjson style @output
     */

    /** Ini to Hjson */
    if ( ini2hj ) {
        /** Load the ini based configuration into @hjCfgMgr */
        wf::config::config_manager_t cfgMgr = wf::config::build_configuration( { PLUGIN_XML_DIR }, SYSCONFDIR "/wayfire/default.ini", argv[ 2 ] );

        /** hjCfgMgr is fully loaded. No initialization required */
        HjsonCodec *codec = new HjsonCodec( &cfgMgr, false );

        /** Dump the configuration to a temporary file */
        char temp[ 24 ] = "/tmp/ini2hj_hsjonXXXXXX";
        int  fd         = mkstemp( temp );

        if ( fd != -1 ) {
            std::string hjson_str = codec->configAsString();
            int         ret       = write( fd, hjson_str.c_str(), hjson_str.size() );

            if ( ret != (ssize_t)hjson_str.size() ) {
                std::cerr << "Error writing to the temp file. Hjson may not be properly formatted." << std::endl;
            }

            close( fd );

            input = temp;
        }

        else {
            std::cerr << "Unable to create temp file. The resulting hjson may not be optimized." << std::endl;
            codec->saveConfigTo( output );
        }
    }

    /** Initialize the HjsonCodec. We will initialize with user provided config later */
    HjsonCodec *codec = new HjsonCodec( &hjCfgMgr, false );

    /** Initialize with user provided config */
    codec->initialize( { PLUGIN_XML_DIR }, SYSCONFDIR "/wayfire/default.hjson", input );

    /** Step 3 of Ini -> Hjson: Hjson -> Hjson */
    if ( ini2hj ) {
        /** Store the config loaded into hjCfgMgr into @output */
        codec->saveConfigTo( output );
    }

    /** Hjson -> Ini Conversion */
    else {
        /** Store the config loaded into hjCfgMgr into @output */
        wf::config::save_configuration_to_file( hjCfgMgr, argv[ 3 ] );
    }

    return 0;
}
