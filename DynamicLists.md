# Dyanmic Lists of Wayfire
Dynamic lists cover the range of user defined options, the number of which is determined at runtime. One of the very good examples is the
options for autostart plugin. The programs, and their number, which are to be auto-started depend on the user. Another example is command
plugin, where the user makes a pair of keybinding and a command to be executed upon activating the said key binding. A third example would
be that of a session manager plugin, which executes a series of actions sequentially: launch an application, maximize the window, and then
send it to 4th workspace.

These varied types of configs are handled by wayfire by making use of `compound_option_t` class. The config data is stored as a list of tuples.
Also stored are the ancilliary information such as the prefixes, and the data type of each of the tuple member (ex: string, activator, etc.).

The challenge is to store this list of tuples, along with other options, in a coherent manner using a file format that any user can intuitively
edit. The simplest format is ini, but saving lists becomes cumbersome. A slightly more advanced format that retain the readbility is the json
format. We have adopted the human-json format (hjson) for further ease of use. Simply stroing this list of tuples as-is, makes it  inconvenient
for editing purposes. Thus, we need some hint to suitably store the config data: `type-hint` attribute for options of `dynamic-list` type.

Currently three type-hints are envisioned, one for each of the scenarios listed above. An option of `type-hint = "plain"` would be stored as
simple array. For example, a sample autostart would look like:
```
autostart: {
    commands: [
        command1
        command2
        ...
    ]
    autostart_wf_shell: false
}
```
The options of the command plugin, with `type-hint = "dict"` will be stored as a dict of dicts.
Dicts can be of two types, (a) single entry, (b) multi-entry.

A typical example of the option `command/bindings` would contain the data:
`[["geany", "/bin/geany", "<super> KEY_G"], ["terminal", "/path/to/desq-term", "<super> KEY_T"], ...]`
This is a multi entry dict.

The extra information to save this data is stored in the xml file in the form of "type-hint" attribute of the compound option.
```
<option name="bindings" type="dynamic-list" type-hint="dict">
    <_short>Regular bindings</_short>
    <_long>Regular bindings</_long>
    <entry prefix="command_" type="string" name="command" />
    <entry prefix="binding_" type="activator" name="binding" />
</option>
```
`compound_option_t` stores this information, and can be accessed via `get_type_hint()` member function.

In case of multi-element dict, while storing the config, the first element of the tuple, is taken as the key of the dict representing the tuple.
The dict will have two entries, one with key `command`, second with key `binding`. Thus the generated config would look like
```
command:{
    bindings: {
        geany: {
            command: /bin/geany
            binding: <super> KEY_G
        }
        terminal: {
            command: /path/to/desq-term
            binding: <super> KEY_T
        }
    }
}
```

In case of vswitch, the xml contains:
```
<option name="workspace_bindings" type="dynamic-list" type-hint="dict">
    <_short>Go-To-Workspace bindings</_short>
    <_long>Go-To-Workspace bindings</_long>
    <entry prefix="binding_" type="activator"/>
</option>
```
We have only one entry specification. This is a single entry dict. In such cases, the loaded data will be similar to
`[["1", "<super> KEY_F1"], ["2", "<super> KEY_F2"]]`.
In such cases, i.e., where the `entry` node does not have a `name` attribute, we will use the first element itself as the key, thus generating
the hjson
```
vswitch:{
    workspace_bindings: {
        1: <super> KEY_F1
        2: <super> KEY_F2
        ...
    }
}
```

Finally, in the thrid scenario, which is of `type-hint = "tuple"`, we'll store the tuples as-is:
```
session: [
    [
        /bin/geany
        on created if app_id is "geany" then maximize
        on created if app_id is "geany" then assign_workspace 0 2
    ]
    [
        /bin/libreoffice --writer
        on created if app_id is "libreoffice-write" then maximize
        on created if app_id is "libreoffice-write" then assign_workspace 0 3
    ]
]
```
