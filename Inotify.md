# Watching the config file for changes

The simplest way to watch for changes is using the inotify API provided by the kernel. When watching the config file,
we have to recieve inotify events and handle them suitably.

1. The config file exists
   a. Modification event (IN_MODIFY)
      In this case, simply read the config file and update the configurations.

   b. File Deletion event (IN_DELETE_SELF/IN_MOVED_FROM/IN_MOVE_SELF)
      All these events are treated in the same way: We will remove the watch on the file. Further, we need to watch the
      parent directory of this file, if it exists, for file creation events (IN_CREATE/IN_MOVED_TO). If the config file
      is recreated, create a new watch for it.

2. The config file does not exist.
   This is equivalent to existing config file being deleted. See 1b. for more details.

3. Edge cases: Currently not handled in a suitable fashion.
   a. The config file and it's parent(s) do not exist. Ideally, we could set up a watch all the way up to /. But practically
      it's pointless.
   b. Ghost events: The parent directory was moved and other extraneous circumstances.
      Since the file is being watched, the underlying inode will not be moved until the watch closes. Additionally, if this file was
      open in an editor, spurious modification events evll be sent. When parent directory is deleted/moved, we stop the watch.

To handle (1) and (2), we do the following:
- Create an inotify instance.
- Add watch for two paths:
  - Config file itself  (IN_MODIFY | IN_DELETE_SELF | IN_MOVE_SELF)
  - Parent directory    (IN_CREATE | IN_DELETE_SELF | IN_MOVED_FROM | IN_MOVE_SELF | IN_MOVED_TO)
- If both do not exist, bail.
- If the config file emits IN_MODIFY, reload the configuration. In all other cases, remove the watch from the config file.
- If the parent directory emits any event other than IN_CREATE/IN_MOVED_FROM, remove the watch on the config file.
  In case of IN_CREATE, if the created file was the config file, then remove the watch on the config file.
- In case of IN_CREATE and IN_MOVED_TO, if the path matches the config file, create new watches for the config file.

Event meanings:
IN_CREATE - A new node was created in the watched directory. event->name contains the name with size event->len.
IN_MODIFY - A watched node was modified. event->name will be empty.
IN_DELETE_SELF - A watched node was deleted. event->name will be empty.
IN_MOVED_FROM - A node was moved from the watched folder. event->name contains the old name with size event->len.
IN_MOVE_SELF - A watched node was moved. event->name will be empty.
IN_MOVED_TO - A node was moved from somewhere into the watched folder. event->name contains the new name with size event->len.
