/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <algorithm>
#include <iostream>
#include <fstream>
#include <memory>
#include <set>

#include <wayfire/util/log.hpp>
#include <wayfire/config/option.hpp>
#include <wayfire/config/section.hpp>
#include <wayfire/config/config-manager.hpp>
#include <wayfire/config/compound-option.hpp>

#include "codec.hpp"
#include "parser/hjson.h"

std::string HjsonCodec::configAsString() {
    Hjson::Value root;

    for ( std::shared_ptr<wf::config::section_t> section: mCfgMgr->get_all_sections() ) {
        std::set<std::string> all_compound_prefixes;
        const auto&           is_part_of_compound_option = [ & ] (const std::string& name) {
                                                               return std::any_of(
                                                                   all_compound_prefixes.begin(), all_compound_prefixes.end(),
                                                                   [ & ] (const auto& prefix){
                                                                       return name.substr( 0, prefix.size() ) == prefix;
                                                                   } );
                                                           };

        root[ section->get_name() ] = Hjson::Value();

        for ( std::shared_ptr<wf::config::option_base_t> option: section->get_registered_options() ) {
            auto compound = std::dynamic_pointer_cast<wf::config::compound_option_t>( option );

            if ( compound ) {
                auto        values   = compound->get_value_untyped();
                const auto& prefixes = compound->get_entries();
                for (auto& p : prefixes) {
                    if ( p->get_prefix().size() ) {
                        all_compound_prefixes.insert( p->get_prefix() );
                    }
                }
            }
        }

        for ( std::shared_ptr<wf::config::option_base_t> option: section->get_registered_options() ) {
            auto compound = std::dynamic_pointer_cast<wf::config::compound_option_t>( option );

            if ( compound ) {
                auto        values    = compound->get_value_untyped();
                std::string type_hint = compound->get_type_hint();

                if ( (type_hint == "plain") or (type_hint == "tuple") ) {
                    root[ section->get_name() ][ compound->get_name() ] = Hjson::Value( Hjson::Type::Vector );
                }

                else if ( type_hint == "dict" ) {
                    root[ section->get_name() ][ compound->get_name() ] = Hjson::Value( Hjson::Type::Map );
                }

                else {
                    LOGE( "Unsupported type hint:", type_hint );
                    continue;
                }

                /**
                 * A simple list of strings.
                 * We need to remove the pseudo option name from each of the entries
                 * Also, we will have two and only two items in each entry
                 */
                if ( type_hint == "plain" ) {
                    for ( std::vector<std::string> entries: values ) {
                        root[ section->get_name() ][ compound->get_name() ].push_back( entries[ 1 ] );
                    }
                }

                /**
                 * A list of list of strings.
                 * We need to remove the pseudo option name from each of the entries
                 * Also, we will have two or more items in each entry
                 */
                else if ( type_hint == "tuple" ) {
                    for ( std::vector<std::string> entries: values ) {
                        Hjson::Value entry;
                        for ( int i = 1; i < (int)entries.size(); i++ ) {
                            entry.push_back( entries[ i ] );
                        }
                        root[ section->get_name() ][ compound->get_name() ].push_back( entry );
                    }
                }

                /**
                 * A dict. Can be a plain dict, or a dict of dicts.
                 * If the dict has one entry, then it's a plain dict.
                 * Otherwise it's a dict of dicts.
                 */
                else {
                    std::vector<std::string> entry_names;
                    for ( auto& entry: compound->get_entries() ) {
                        entry_names.push_back( entry->get_name() );
                    }

                    /** Plain dict */
                    if ( entry_names.size() == 1 ) {
                        for ( std::vector<std::string> key_value_pairs: values ) {
                            if ( not key_value_pairs.size() ) {
                                continue;
                            }

                            root[ section->get_name() ][ compound->get_name() ][ key_value_pairs[ 0 ] ] = key_value_pairs[ 1 ];
                        }
                    }

                    /** A dict of dicts */
                    else {
                        for ( std::vector<std::string> entry_values: values ) {
                            if ( not entry_values.size() ) {
                                continue;
                            }

                            Hjson::Value dict = Hjson::Value( Hjson::Type::Map );
                            for ( int i = 1; i < (int)entry_values.size(); i++ ) {
                                std::string key = entry_names[ i - 1 ];
                                key         = (key.size() ? key : "elm" + std::to_string( i ) );
                                dict[ key ] = entry_values[ i ];
                            }
                            root[ section->get_name() ][ compound->get_name() ][ entry_values[ 0 ] ] = dict;
                        }
                    }
                }

                /** The next part is for normal options, so continue to next option */
                continue;
            }

            if ( not is_part_of_compound_option( option->get_name() ) ) {
                auto opt_bool = std::dynamic_pointer_cast<wf::config::option_t<bool> >( option );
                auto opt_int  = std::dynamic_pointer_cast<wf::config::option_t<int> >( option );
                auto opt_dbl  = std::dynamic_pointer_cast<wf::config::option_t<double> >( option );

                if ( opt_bool ) {
                    root[ section->get_name() ][ option->get_name() ] = opt_bool->get_value();
                }

                else if ( opt_int ) {
                    root[ section->get_name() ][ option->get_name() ] = opt_int->get_value();
                }

                else if ( opt_dbl ) {
                    root[ section->get_name() ][ option->get_name() ] = opt_dbl->get_value();
                }

                else {
                    root[ section->get_name() ][ option->get_name() ] = option->get_value_str();
                }
            }
        }
    }

    Hjson::EncoderOptions encOpts;

    encOpts.indentBy      = "\t";
    encOpts.unknownAsNull = true;

    std::string hjsonStr = Hjson::Marshal( root, encOpts );

    return hjsonStr;
}


bool HjsonCodec::saveConfigTo( std::string cfgname ) {
    /** If we have an empty filename, use the user configuration file */
    if ( not cfgname.size() ) {
        cfgname = usrCfgFile;
    }

    std::string hjsonStr = configAsString();

    if ( cfgname.size() ) {
        std::ofstream hjf;
        hjf.open( cfgname, std::ofstream::out );
        hjf << hjsonStr << "\n";
        hjf.close();
    }

    else {
        std::cout << hjsonStr << "\n" << std::endl;
    }

    return true;
}
