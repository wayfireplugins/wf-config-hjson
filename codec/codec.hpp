/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <vector>
#include <string>

namespace wf {
    namespace config {
        class config_manager_t;
    }
}

class HjsonCodec {
    public:

        /**
         * @cfgMgr - The configuration manager object. All configuration will be loaded into/saved from
         *this.
         * If @init == true, initialize() will be called, thus loading options from default locations.
         */
        HjsonCodec( wf::config::config_manager_t *cfgMgr, bool init = true );

        /**
         * Initialize the configuration by reloading all the xml files.
         * Calling this will overwrite the existing configuration (if any).
         * @xmlPaths
         *    Read all the xml files in these folders and build the default configuration
         *    If empty, read the XMLs from PLUGIN_XML_DIR defined in wayfire/config.h
         * @sysConf - System configuration file, typically, /etc/xdg/wayfire.hjson
         *    If specified, update the default config, with the one shipped by the OS/Distro/etc.
         *    If empty, obtain the default one from SYSCONFDIR /wayfire/default.hjson (if it exists)
         *    Pass '-' to prevent loading @sysConf.
         * @usrConf - User configuration file, typically, ~/.config/wayfire.hjson
         *    If specified, update the current config, with the user-defined one.
         *    If empty, first check WAYFIRE_CONFIG_FILE env. Then check ~/.config/wayfire.hjson
         *    Pass '-' to prevent loading @usrConf.
         */
        void initialize( std::vector<std::string> xmlPaths = {}, std::string sysConf = "", std::string usrConf = "" );

        /**
         * Load configuration options from @hjFile. This is over and above @sysConf and @usrConf, if they
         *were loaded.
         * Calling this without initialization will not load any options
         */
        bool loadConfigFrom( std::string hjFile );

        /**
         * Save the configuration loaded in @cfgMgr to @hjFile.
         * If no name is specified, then the file will be written to @usrConf
         * If @usrConf invalid during initialize(...), the config will be written to ~/.config/wayfire.hjson
         */
        bool saveConfigTo( std::string hjFile = "" );

        /**
         * Read the config from std::string
         */
        bool configFromString( std::string config );

        /**
         * Return the config as std::string
         */
        std::string configAsString();

        /**
         * Handlle the inotify events suitably.
         * Read Inotify.md for more details.
         */
        static int handleInotifyEvents( int fd, uint32_t mask, void *data );

        /** The user configuration file */
        std::string usrCfgFile;

        /** The inotifyFD: This will be passed to wl_event_loop_add(...) */
        int inotifyFD = -1;

    private:
        wf::config::config_manager_t *mCfgMgr;

        /** Watch descriptors */
        int usrCfgWD = -1;      // WD for the user config file
        int parDirWD = -1;      // WD for the parent directory
};
