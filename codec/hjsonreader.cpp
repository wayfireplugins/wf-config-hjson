/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <memory>
#include <fstream>
#include <iostream>

#include <wayfire/util/log.hpp>
#include <wayfire/config/option.hpp>
#include <wayfire/config/section.hpp>
#include <wayfire/config/config-manager.hpp>
#include <wayfire/config/compound-option.hpp>

#include "codec.hpp"
#include "parser/hjson.h"

bool HjsonCodec::loadConfigFrom( std::string cfgname ) {
    std::string hj_str;

    std::ifstream hjson( cfgname );

    std::stringstream ss;

    ss << hjson.rdbuf();

    hj_str = ss.str();
    hjson.close();

    return configFromString( hj_str );
}


bool HjsonCodec::configFromString( std::string config ) {
    Hjson::Value root;

    /** Load the Hjson config file */
    try {
        root = Hjson::Unmarshal( config );
    } catch ( Hjson::syntax_error& err ) {
        LOGW( "Error loading user configuration file. Invalid syntax:" );
        LOGW( err.what() );
        return false;
    } catch ( Hjson::file_error& err ) {
        LOGW( "Error loading user configuration file. Unable to open file:" );
        LOGW( err.what() );
        return false;
    }

    for ( std::shared_ptr<wf::config::section_t> section: mCfgMgr->get_all_sections() ) {
        for ( std::shared_ptr<wf::config::option_base_t> option: section->get_registered_options() ) {
            /** We'll deal with compound options later. */
            auto compound = std::dynamic_pointer_cast<wf::config::compound_option_t>( option );

            if ( compound ) {
                continue;
            }

            /** The option is loacked. Do nothing. */
            if ( option->is_locked() ) {
                continue;
            }

            /** Update only if the option is valid. Otherwise, go ahead */
            if ( root[ section->get_name() ][ option->get_name() ].defined() ) {
                option->set_value_str( root[ section->get_name() ][ option->get_name() ].to_string() );
            }
        }
    }

    for ( std::shared_ptr<wf::config::section_t> section: mCfgMgr->get_all_sections() ) {
        for ( std::shared_ptr<wf::config::option_base_t> option: section->get_registered_options() ) {
            /** We'll deal with compound options later. */
            auto compound = std::dynamic_pointer_cast<wf::config::compound_option_t>( option );

            if ( compound ) {
                Hjson::Value values = root[ section->get_name() ][ option->get_name() ];

                if ( not values.defined() ) {
                    continue;
                }

                if ( not values.size() ) {
                    continue;
                }

                Hjson::Type optionType = values.type();
                std::vector<std::vector<std::string> > opt_values;

                /** type-hint == "plain". Add a pseudo option name */
                if ( optionType == Hjson::Type::Vector ) {
                    /** Determine if we have a plain list or a list of tuples */
                    std::string type_hint = compound->get_type_hint();
                    bool        plain     = true;

                    if ( type_hint.compare( "plain" ) == 0 ) {
                        plain = true;
                    }

                    else if ( type_hint.compare( "tuple" ) == 0 ) {
                        plain = false;
                    }

                    else {
                        LOGW( "Invalid type hint. Expecting 'plain' or 'tuple'. Got ", type_hint );
                        LOGW( "Guessing compound option type." );

                        if ( values[ 0 ].type() == Hjson::Type::Vector ) {
                            plain = false;
                        }

                        else {
                            plain = true;
                        }
                    }

                    for ( int i = 0; i < (int)values.size(); i++ ) {
                        Hjson::Value             value = values[ i ];
                        std::vector<std::string> new_entry;

                        /**
                         * Generate a pseudo-name, before updating the option
                         */
                        new_entry.push_back( "opt_" + std::to_string( i ) );

                        if ( plain ) {
                            new_entry.push_back( value.to_string() );
                        }

                        else {
                            for ( int j = 0; j < (int)value.size(); j++ ) {
                                new_entry.push_back( value[ j ].to_string() );
                            }
                        }

                        opt_values.push_back( new_entry );
                    }
                }

                /** type-hint == "dict" */
                else if ( optionType == Hjson::Type::Map ) {
                    /** Determine if we have a plain dict, or a dict of dicts */
                    bool plain = true;

                    if ( values[ values.key( 0 ) ].type() == Hjson::Type::Map ) {
                        plain = false;
                    }

                    for ( int i = 0; i < (int)values.size(); i++ ) {
                        /** Get the key */
                        std::string key = values.key( i );
                        /** Get the value for the key */
                        Hjson::Value value = values.at( key );
                        /** Vector to store the config options */
                        std::vector<std::string> new_entry;

                        /**
                         * Push the key into the vector
                         */
                        new_entry.push_back( key );

                        /** Simple dict */
                        if ( plain ) {
                            new_entry.push_back( value.to_string() );
                        }

                        /** Dict of dicts; value.type() == Hjson::Type::Map */
                        else {
                            for ( int j = 0; j < (int)value.size(); j++ ) {
                                new_entry.push_back( value.at( value.key( j ) ).to_string() );
                            }
                        }

                        opt_values.push_back( new_entry );
                    }
                }

                else {
                    LOGE( option->get_name(), ": Expected Vector/Map. Got ", (int)optionType );
                }

                compound->set_value_untyped( opt_values );
            }
        }
    }

    return true;
}
