/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <dirent.h>
#include <unistd.h>
#include <sys/inotify.h>

#include <filesystem>
#include <cstring>
#include <set>

#include <wayland-server-core.h>

#include <wayfire/config.h>
#include <wayfire/util/log.hpp>
#include <wayfire/config/xml.hpp>
#include <wayfire/config/config-manager.hpp>

#include "codec.hpp"

namespace fs = std::filesystem;

#define MAX_EVENTS    1024
#define EVENT_SIZE    (sizeof(struct inotify_event) )
#define BUF_LEN       (MAX_EVENTS * (EVENT_SIZE + NAME_MAX + 1) )

/**
 * Parse the XML file and return the node which corresponds to the section.
 */
static xmlNodePtr find_section_start_node( const std::string& file ) {
    auto doc = xmlParseFile( file.c_str() );

    if ( !doc ) {
        LOGE( "Failed to parse XML file ", file );
        return nullptr;
    }

    auto root = xmlDocGetRootElement( doc );

    if ( !root ) {
        LOGE( file, ": missing root element." );
        xmlFreeDoc( doc );
        return nullptr;
    }

    /* Seek the plugin section */
    auto section = root->children;

    while ( section != nullptr ) {
        if ( (section->type == XML_ELEMENT_NODE) &&
             ( ( (const char *)section->name == (std::string)"plugin") ||
               ( (const char *)section->name == (std::string)"object") ) ) {
            break;
        }

        section = section->next;
    }

    return section;
}


static wf::config::config_manager_t load_xml_files( const std::vector<std::string>& xmldirs ) {
    wf::config::config_manager_t manager;

    for (auto& xmldir : xmldirs) {
        auto xmld = opendir( xmldir.c_str() );

        if ( NULL == xmld ) {
            LOGW( "Failed to open XML directory ", xmldir );
            continue;
        }

        LOGI( "Reading XML configuration options from directory ", xmldir );
        struct dirent *entry;
        while ( (entry = readdir( xmld ) ) != NULL ) {
            if ( (entry->d_type != DT_LNK) && (entry->d_type != DT_REG) ) {
                continue;
            }

            std::string filename = xmldir + '/' + entry->d_name;

            if ( (filename.length() > 4) &&
                 (filename.rfind( ".xml" ) == filename.length() - 4) ) {
                LOGI( "Reading XML configuration options from file ", filename );
                auto node = find_section_start_node( filename );

                if ( node ) {
                    manager.merge_section(
                        wf::config::xml::create_section_from_xml_node( node ) );
                }
            }
        }

        closedir( xmld );
    }

    return manager;
}


HjsonCodec::HjsonCodec( wf::config::config_manager_t *cfgMgr, bool init ) {
    /** Store the pointer for later use */
    mCfgMgr = cfgMgr;

    if ( init ) {
        initialize();
    }
}


void HjsonCodec::initialize( std::vector<std::string> xmlPaths, std::string sysConf, std::string usrConf ) {
    if ( not xmlPaths.size() ) {
        xmlPaths.push_back( PLUGIN_XML_DIR );
    }

    /** Parse the xml files and store the resulting configuration in @mCfgMgr */
    *mCfgMgr = load_xml_files( xmlPaths );

    /**
     * Load @sysConf hjson
     */

    /** Empty string: Try default value */
    if ( not sysConf.size() ) {
        sysConf = SYSCONFDIR "/wayfire/default.hjson";
    }

    /** Load @sysConf if it exists */
    if ( fs::exists( sysConf ) ) {
        if ( loadConfigFrom( sysConf ) ) {
            LOGI( "Loaded configurations from ", sysConf );
        }

        else {
            LOGW( "Failed to load configurations from ", sysConf );
        }
    }

    else {
        if ( sysConf == "-" ) {
            LOGI( "Skip loading system default configurations" );
        }

        else {
            LOGW( "Invalid system default configuration file ", sysConf );
        }
    }

    /**
     * Load @usrConf hjson
     */

    /** Empty string: Initialize to default value and store it */
    if ( not usrConf.size() ) {
        usrCfgFile = usrConf = std::string( getenv( "HOME" ) ) + "/.config/wayfire.hjson";
    }

    else {
        /** Skip reading the usrConf, but usrCfgFile will be init to default user config */
        if ( usrConf == "-" ) {
            usrCfgFile = std::string( getenv( "HOME" ) ) + "/.config/wayfire.hjson";
        }

        else {
            usrCfgFile = usrConf;
        }
    }

    /** Load @sysConf if it exists */
    if ( fs::exists( usrConf ) ) {
        if ( loadConfigFrom( usrConf ) ) {
            LOGI( "Loaded configurations from ", usrConf );
        }

        else {
            LOGW( "Failed to load configurations from ", usrConf );
        }
    }

    else {
        if ( usrConf == "-" ) {
            LOGI( "Skip loading user configurations" );
        }

        else {
            LOGW( "Invalid user configuration file ", usrConf );
        }
    }

    /**
     * Watch the config for changes
     * Start the watch only if @usrConf was specified or empty.
     * If usrConf == "-", i.e., if we were to explicitly ignore @usrConf,
     * We will not start the watch.
     */

    if ( usrConf == "-" ) {
        /** Nothing to do! */
        return;
    }

    /** Initialize the inotify instance */
    inotifyFD = inotify_init1( IN_CLOEXEC );

    if ( inotifyFD == -1 ) {
        LOGE( "Failed to initialize inotify instance:", strerror( errno ) );
    }

    /** Add watch on the usrCfgFile if it exists */
    if ( fs::exists( usrCfgFile ) ) {
        usrCfgWD = inotify_add_watch(
            inotifyFD,                                  // Inotify FD
            usrCfgFile.c_str(),                         // Path of the user config file
            IN_MODIFY | IN_DELETE_SELF | IN_MOVE_SELF   // Events to watch for
        );

        if ( usrCfgWD == -1 ) {
            LOGE( "Failed to add ", usrCfgFile, " to inotify watch:", strerror( errno ) );
        }
    }

    /** Add watch on the parent directory */
    std::string usrCfgDir = fs::path( usrCfgFile ).parent_path();
    parDirWD = inotify_add_watch(
        inotifyFD,                                                  // Inotify FD
        usrCfgDir.c_str(),                                          // Path of the user config file's parent
        IN_CREATE | IN_DELETE_SELF | IN_MOVE_SELF | IN_MOVED_TO     // EVents to watch for
    );

    if ( parDirWD == -1 ) {
        LOGE( "Failed to add parent directory, ", usrCfgDir, " to inotify watch:", strerror( errno ) );
    }

    /** The only instance when parDirWD == usrCfgWD is when both are -1 => No watch needed. */
    if ( parDirWD == usrCfgWD ) {
        close( inotifyFD );
        LOGE( "Nothing to watch. Closing the inotify FD." );
    }
}


int HjsonCodec::handleInotifyEvents( int fd, uint32_t mask, void *data ) {
    HjsonCodec *codec = (HjsonCodec *)data;

    if ( not codec ) {
        return 0;
    }

    if ( (fd != codec->inotifyFD) or (mask != WL_EVENT_READABLE) ) {
        return 0;
    }

    char buffer[ BUF_LEN ] = { 0 };
    int  length            = read( codec->inotifyFD, buffer, BUF_LEN );

    int i = 0;

    /** If for some reason length <= 0, we have nothing to go on */
    if ( length <= 0 ) {
        return 0;
    }

    while ( i < length ) {
        struct inotify_event *event = ( struct inotify_event * )&buffer[ i ];

        /* WD of the current event */
        int curWD = event->wd;

        /** If this wd is not usrCfgWD or parDirWD, we go to the next event */
        if ( (curWD != codec->usrCfgWD) and (curWD != codec->parDirWD) ) {
            LOGE( "We should have never reached this point. event->wd, usrCfgWD, parDirWD: ", event->wd, " ", codec->usrCfgWD, " ", codec->parDirWD );
            i += EVENT_SIZE + event->len;
            continue;
        }

        /** Handle events of the user config file */
        if ( curWD == codec->usrCfgWD ) {
            if ( event->mask & IN_MODIFY ) {
                LOGI( "Reloading the config file ", codec->loadConfigFrom( codec->usrCfgFile ) );
            }

            else {
                inotify_rm_watch( codec->inotifyFD, codec->usrCfgWD );
                codec->usrCfgWD = -1;
                LOGI( "Removing watch on ", codec->usrCfgFile );
            }
        }

        /** Events for the parent dir */
        else {
            /** A file was created or movied into this folder */
            if ( (event->mask & IN_CREATE) or (event->mask & IN_MOVED_TO) ) {
                /** The file name matched */
                if ( strcmp( event->name, fs::path( codec->usrCfgFile ).filename().c_str() ) == 0 ) {
                    codec->usrCfgWD = inotify_add_watch(
                        codec->inotifyFD,                           // Inotify FD
                        codec->usrCfgFile.c_str(),                  // Path of the user config file
                        IN_MODIFY | IN_DELETE_SELF | IN_MOVE_SELF   // EVents to watch for
                    );
                    codec->loadConfigFrom( codec->usrCfgFile );
                    LOGI( "Config file added. Creating watch ", codec->usrCfgFile );
                }
            }

            /** The config file was moved */
            else if ( event->mask & IN_MOVED_FROM ) {
                if ( strcmp( event->name, fs::path( codec->usrCfgFile ).filename().c_str() ) == 0 ) {
                    inotify_rm_watch( codec->inotifyFD, codec->usrCfgWD );
                    codec->usrCfgWD = -1;

                    LOGI( "Config file was moved. Removing watch on ", codec->usrCfgFile );
                }
            }

            /** Parent was moved/deleted. */
            else {
                /** We may not be watching the usrCfgFile when the parent was moved/deleted */
                if ( codec->usrCfgWD != -1 ) {
                    inotify_rm_watch( codec->inotifyFD, codec->usrCfgWD );
                    codec->usrCfgWD = -1;

                    LOGI( "Config file vanished. Removing watch on ", codec->usrCfgFile );
                }

                if ( (event->mask & IN_DELETE_SELF) or (event->mask & IN_MOVE_SELF) ) {
                    inotify_rm_watch( codec->inotifyFD, codec->parDirWD );
                    codec->parDirWD = -1;

                    LOGW( "Parent dir vanished. Closing watch." );

                    /** Nothing more to watch. Close the inotify instance. */
                    close( codec->inotifyFD );

                    /** No more checks required */
                    return 0;
                }
            }
        }

        i += EVENT_SIZE + event->len;
    }

    return 0;
}
